# Create empty files

For a lark I wanted to create a file system with a billion empty
files. I wrote the program in this repository to do that. It creates
an empty, sparse file, puts an ext4 file system in that, mounts it,
and then creates as many empty files as requested.

When I ran it, it took about 26 hours to run. The resulting file takes
276 GiB of disk space.

## notes on usage
If you want to try another file system type, if you manually create them, this will detect and use them, as long as `mount` automatically recognizes them.

You should run this on a file system that supports sparse files.
If it takes over a minute to show a progress meter, check this.

* good: ext4, btrfs
* bad: HFS+
